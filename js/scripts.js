const body = document.querySelector('BODY');
const button = document.getElementById('calculadora')
const pantalla = document.getElementById('pantalla')
const resultado = document.getElementById('resultado')
const lottie = document.getElementById('lottie')
const checkbox = document.querySelector('input[name="checkbox"]');
/*console.log(checkbox[0].checked)*/
var temporal;
var strNumero='';
var primerValor;
var resultadoIgual;
var operacion;
for (const key in button.querySelectorAll('.boton')) {
    console.log(button.querySelectorAll('.boton')[key])
}
checkbox.addEventListener('change',function(){
    if (this.checked) {
        console.log("DARK THEME")
        pantalla.classList.replace('blanco','negro')
        body.classList.replace('light','dark')
        for (const key in button.querySelectorAll('.boton')) {
            try{
                button.querySelectorAll('.boton')[key].classList.replace('morado-suave','morado')
            }catch (error){}
        }
    }else{
        
        console.log("LIGHT THEME")
        pantalla.classList.replace('negro','blanco')
        body.classList.replace('dark','light')
        for (const key in button.querySelectorAll('.boton')) {
            try {
                button.querySelectorAll('.boton')[key].classList.replace('morado','morado-suave')
            } catch (error) {} 
        }
    }
    console.log(this.checked)
})

button.addEventListener('click',(e)=>{
    console.log(e.target.attributes[0].nodeValue)
    console.log(e.target.innerText)
    
    if (e.target.attributes[0].nodeValue!='calculadora') {
        if (strNumero.length<=18) {
            temporal = e.target.innerText
            pantalla.innerText += temporal
            strNumero += temporal
        
        }
        if (e.target.attributes[0].nodeValue=='limpiar') {
            pantalla.innerText = ''
        }
        if (e.target.attributes[0].nodeValue=='sumar') {
            primerValor = parseInt(pantalla.innerText)
            resultado.querySelector('span').innerText = 'SUMANDO'
            pantalla.innerText = ''
            operacion = 'suma'
            activarLottie()
        }
        if (e.target.attributes[0].nodeValue=='restar') {
            primerValor = parseInt(pantalla.innerText)
            resultado.querySelector('span').innerText = 'RESTANDO'
            pantalla.innerText = ''
            operacion = 'resta'
            activarLottie()
        }
        if (e.target.attributes[0].nodeValue=='multiplicar') {
            primerValor = parseInt(pantalla.innerText)
            resultado.querySelector('span').innerText = 'MULTIPLICANDO'
            pantalla.innerText = ''
            operacion = 'multiplicacion'
            activarLottie()
        }
        if (e.target.attributes[0].nodeValue=='dividir') {
            primerValor = parseInt(pantalla.innerText)
            resultado.querySelector('span').innerText = 'DIVIDIENDO'
            pantalla.innerText = ''
            operacion = 'division'
            activarLottie()
        }
        if (e.target.attributes[0].nodeValue=='igual') {
            desactivarLottie()
            switch (operacion) {
                case 'suma':
                    resultadoIgual = primerValor+parseInt(pantalla.innerText)
                    primerValor = resultadoIgual
                    pantalla.innerText =  resultadoIgual
                    resultado.querySelector('span').innerText = ''
                    break
                case 'resta':
                    resultadoIgual = primerValor-parseInt(pantalla.innerText)
                    primerValor = resultadoIgual
                    pantalla.innerText =  resultadoIgual
                    resultado.querySelector('span').innerText = ''
                    break
                case 'multiplicacion':
                    resultadoIgual = primerValor*parseInt(pantalla.innerText)
                    primerValor = resultadoIgual
                    pantalla.innerText =  resultadoIgual
                    resultado.querySelector('span').innerText = ''
                    break
                case 'division':
                    resultadoIgual = primerValor/parseInt(pantalla.innerText)
                    primerValor = resultadoIgual
                    pantalla.innerText =  resultadoIgual
                    resultado.querySelector('span').innerText = ''
                    break
                default:
                    
            }
            
        }
    }
    
})

function activarLottie(){
    lottie.classList.remove('ocultar')
}
function desactivarLottie(){
    lottie.classList.add('ocultar')
}
const sumar = (a,b)=>{
    return parseInt(a)+parseInt(b)
}
console.log(`la suma de ${1}+${3} es igual a ${sumar(1,3)}`)